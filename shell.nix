{pkgs ? import <nixpkgs> {config = {allowUnfree = true;};}}:
pkgs.mkShell {
  packages = [
    pkgs.texlive.combined.scheme-full
  ];
}
